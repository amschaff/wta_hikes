# README #

### WTA_Hikes ###
* Intro to Graph Databases using Neo4j and Python
* Slides and Python code from the PyLadies Seattle talk night presentation on March 22, 2017
* Version 1.0

### How do I get set up? ###

* Install Neo4j (developed on Neo4j 3.1.2)
* Developed Using Python 3.5.3, Continuum Analytics build
  * Python packages installed using conda

* Python Execution Order:  
1. create_constraints.py  
2. load_hikes_to_neo4j.py  
3. recommend_hike.py - recommends a hike based on a seed hike and similar region & features  
4. hike_difficulty.py - adds difficulty level to database  
5. recommend_by_difficulty.py - returns 10 random hikes based on a region & difficulty level


### Sample data for this project is from the Washington Trails Association.
### If you like this project, please consider donating to the WTA: http://wta.org/donate