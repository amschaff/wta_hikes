# Find the top five hikes in the same region
# and with at least two features in common with the given hike

import sys
import random
import pprint
from neo4j.v1 import GraphDatabase, basic_auth
import csv

neodriver = GraphDatabase.driver("bolt://localhost:7687",
                                 auth=basic_auth("neo4j", "neo4j_2"))

data_dir = '../data/'


def get_all_hike_difficulty_data():
    with neodriver.session() as session:
        tx = session.begin_transaction()
        qry = '''MATCH (h:Hike)
                 RETURN h.name AS name,
                        h.distance AS distance,
                        h.elevation_gain as gain
              '''
        return list(tx.run(qry))


def assign_difficulty(distance, gain):
    difficulty = 4
    if distance <= 4 and gain <= 1000:
        difficulty = 1
    elif distance <= 8 and gain <= 2500:
        difficulty = 2
    elif distance <= 12 and gain <= 4000:
        difficulty = 3
    return difficulty


# persist results to neo4j
def create_difficulty_relationships(hike_data):
    insert_query = '''
    WITH {hike_name} as hike_name, {difficulty} as difficulty
    MATCH (h:Hike {name: hike_name})
    MERGE (h)-[:IS_LEVEL]->(d:Difficulty {level: difficulty})
    '''
    with neodriver.session() as session:
        tx = session.begin_transaction()
        for h in hike_data:
            difficulty = assign_difficulty(float(h['distance']),
                                           int(h['gain']))
            tx.run(insert_query,
                   parameters={'hike_name': h['name'],
                               'difficulty': difficulty}).consume()
        tx.commit()


# persist results to file for charting
def write_results_to_file(hike_data):
    with open(data_dir + 'hike_difficulty.csv', 'w') as csvfile:
        fwriter = csv.writer(csvfile, delimiter=';')
        fwriter.writerow(['distance', 'gain', 'difficulty'])
        for h in hike_data:
            dis = h['distance']
            g = h['gain']
            fwriter.writerow([dis, g, assign_difficulty(float(dis), int(g))])


def main(argv):
    hike_data = get_all_hike_difficulty_data()
    # write_results_to_file(hike_data)

    create_difficulty_relationships(hike_data)


if __name__ == "__main__":
    main(sys.argv[1:])
