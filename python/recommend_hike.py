# Find the top five hikes in the same region
# and with at least two features in common with the given hike

import sys
import random
import pprint
from py2neo import Graph

# seed_hike = 'Fourth of July Pass via Thunder Creek'
seed_hike = 'Melakwa Lake'
_graph = Graph(host="localhost", user="neo4j", password="neo4j_2")


def get_similar_hikes(hike_name):
    qry = """MATCH (f:Feature)-[]-(h:Hike {name:"%s"})-[]-(r:Region)
        WITH f,r
        MATCH (f)-[hf]-(h2:Hike)-[]-(r)
        WITH collect(f.name) as Features,
             count(hf) as n_features, h2
        WHERE n_features > 1
        AND h2.name <> "%s"
        RETURN h2.name as HikeName,
               h2.rating as Rating
        ORDER by Rating desc
        LIMIT 10""" % (hike_name, hike_name)

    # run(qry).data() method extracts the query results into a list of dictionaries
    # by default, run(qry) returns a cursor
    results = _graph.run(qry).data()
    return results


def get_hike_details(hike_name):
    qry = """MATCH (h:Hike {name:"%s"})
             RETURN h""" % (hike_name)
    results = _graph.data(qry)
    return results


def main(argv):
    hikes = get_similar_hikes(seed_hike)
    print('class of query result is: ' + str(type(hikes)))
    print('\nRecommended Hikes, given that you like ' + seed_hike + ':\n')
    for h in hikes:
        print(h)
    # print(hikes)
    # print_schedules(schedules, 10)

    selected_hike = random.choice(hikes)
    print('\nSelected Hike: ', selected_hike)
    # print(type(selected_hike))
    hike_details = get_hike_details(selected_hike['HikeName'])
    print("Selected hike_details:\n" + str(hike_details))

    # examine the Node object returned from get_hike_details
    print('\nExamining the py2neo data types:')
    hike_node = hike_details[0]['h']
    print(type(hike_node))
    print(hike_node)

    print('\nNode labels:')
    print(hike_node.labels())
    print('\nNode properties:')
    pprint.pprint(hike_node.properties, width=50)


if __name__ == "__main__":
    main(sys.argv[1:])
