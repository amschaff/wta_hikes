# Find the top five hikes in the same region
# and with at least two features in common with the given hike

import sys
from neo4j.v1 import GraphDatabase, basic_auth


neodriver = GraphDatabase.driver("bolt://localhost:7687",
                                 auth=basic_auth("neo4j", "neo4j_2"))


def get_hikes_by_region_difficulty(region_name, difficulty_level):
    with neodriver.session() as session:
        tx = session.begin_transaction()
        qry = '''
            WITH {region} as region, {difficulty} as difficulty
            MATCH (Difficulty {level:difficulty})-[]-(h:Hike)-[]-
                    (r:Region {name:region})
            WITH COLLECT(h) as hikes
                UNWIND apoc.coll.randomItems(hikes, 10) as random_hikes
                RETURN random_hikes.name as name,
                       random_hikes.elevation_gain as gain,
                       random_hikes.distance as distance
        '''
    with neodriver.session() as session:
        tx = session.begin_transaction()
        return list(tx.run(qry, parameters={'region': region_name,
                                            'difficulty': difficulty_level}))


def main(argv):
    region = "North Cascades"
    difficulty_level = 3

    random_hikes = get_hikes_by_region_difficulty(region, difficulty_level)

    print('Hikes selected for %s with difficulty level %i:' %
          (region, difficulty_level))

    for r in random_hikes:
        print('%s: %s miles, %s ft elevation gain' %
              (r['name'], r['distance'], r['gain']))


if __name__ == "__main__":
    main(sys.argv[1:])
