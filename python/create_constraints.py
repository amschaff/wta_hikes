# creates unique constraints based on dict with { Node : property } pairs

import sys
from neo4j.v1 import GraphDatabase, basic_auth

neodriver = GraphDatabase.driver("bolt://localhost:7687",
                                 auth=basic_auth("neo4j", "neo4j_2"))

node_keys = {'Hike': 'name', 'Region': 'name', 'Feature': 'name'}


def main(argv):
    with neodriver.session() as session:
        for k, v in node_keys.items():
            stmt = 'CREATE CONSTRAINT ON (n:'+k+') ASSERT n.'+v+' IS UNIQUE'
            print(stmt)
            session.run(stmt)


if __name__ == "__main__":
    main(sys.argv[1:])
