# imports hikes from json file and imports to neo4j

import sys
import json
from neo4j.v1 import GraphDatabase, basic_auth

neodriver = GraphDatabase.driver("bolt://localhost:7687",
                                 auth=basic_auth("neo4j", "neo4j_2"))

data_dir = '../data/'
data_file_in = 'hike_list_sample.json'

node_keys = {"Hike": "name", "Region": "name", "Feature": "name"}
critical_features = {'name', 'distance', 'gain', 'features',
                     'high_point', 'rating', 'region', 'type'}


def main(argv):
    # load json array into variable
    hikes = load_from_file()
    # print(json.dumps(hikes,indent=4, separators=(',', ': ')))
    # print('hikes: ' + str(len(hikes)))
    hikes, regions, features = clean_hike_data(hikes)
    # print('clean hikes: ' + str(len(hikes)))
    # print('\n')

    load_features(features)
    load_regions(regions)
    load_hikes(hikes)


def load_from_file():
    with open(data_dir + data_file_in) as file:
        hikes = json.load(file)
        return hikes


# include only hikes with all the characteristics i care about
# create sets with regions and features
# split region into region & subregion
def clean_hike_data(hikes):
    regions = set()
    features = set()
    clean_hikes = [h for h in hikes
                   if (set(h.keys()).issuperset(critical_features) and
                       h['type'] == 'Roundtrip')]

    for h in clean_hikes:
        features = features.union(set(h['features']))
        if '--' in h['region']:
            r, s = h['region'].split('--', 1)
            h['region'] = r.strip()
            h['subregion'] = s.strip()
        regions.add(h['region'])

    return clean_hikes, regions, features


# relatively few regions, so create nodes for them ahead of time
# small number: demo explicit transactions
def load_regions(regions):
    with neodriver.session() as session:
        tx = session.begin_transaction()
        for r in regions:
            insert_query = '\nCREATE (r:Region {name:"' + r + '"}) RETURN r'
            print (insert_query)

            result = tx.run(insert_query)
            for record in result:
                print(", ".join("%s: %s" %
                                (key, record[key]) for key in record.keys()))
        tx.commit()


# relatively few features, so create nodes for them ahead of time
# small number, so let it auto-commit after the batch
def load_features(features):
    with neodriver.session() as session:
        for f in features:
            insert_query = '\nCREATE (f:Feature {name:"' + f + '"}) RETURN f'
            print (insert_query)

            result = session.run(insert_query)
            for record in result:
                print(", ".join("%s: %s" %
                                (key, record[key]) for key in record.keys()))


# batch inserts into transactions with 500 hikes each
def load_hikes(hikes):
    insert_query = '''
    WITH {hike} AS hike
    CREATE (h:Hike {name: hike.name})
    SET h.distance = hike.distance,
        h.elevation_gain = hike.gain,
        h.high_point = hike.high_point,
        h.rating = hike.rating
    WITH *
    UNWIND hike.features AS feat
    MATCH (f:Feature {name: feat})
    MERGE (h)-[:HAS_FEATURE]->(f)
    WITH h, hike
    MATCH (r:Region {name: hike.region})
    MERGE (h)-[:LOCATED_IN]->(r)
    '''

    with neodriver.session() as session:
        tx = session.begin_transaction()
        count = 0
        for h in hikes:
            count += 1
            tx.run(insert_query,
                   parameters={'hike': h}).consume()
            if count > 500:
                tx.commit()
                tx = session.begin_transaction()
                count = 0
        tx.commit()


if __name__ == "__main__":
    main(sys.argv[1:])
